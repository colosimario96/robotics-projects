clear;
clc;

% Variables
bottleCoords = [0,0,0.06];
bottleOrientation = [0,0,0];
bottleDynamicsCoords = [0.4, -1.275, 0.175];
bottleDynamicsOrientation = [0, 0, 0];

boxCoords = [1.425, -1.6808, 0.28867];
boxOrientation = [90, 0, 90];
boxDynamicsCoords = [1.425, -1.7059, 0.12487];
boxDynamicsOrientation = [0, 0, 0];

isCapping = 0;
lastCapped = -1;
step = 0;
startCapping = -1;

isMoving = 0;
stepMoving = 0;
bottleToPick = [];
bottleModelToPick = [];
pickedBottle = 0;

coords = [
    1.1784, -1.1696, 0.175, 0, 0, 0;
    1.1588, -1.275, 0.16706, 0, 0, 0;
    1.4034, -1.1196, 0.3, 0, 0, 0;
];
bottleBoxCoords = [
 1.4608, -1.175, 0.21706, 0, 0, 0;
 1.4608, -1.075, 0.21706, 0, 0, 0;
 1.4088, -1.175, 0.21706, 0, 0, 0;
 1.4088, -1.075, 0.21706, 0, 0, 0;
];

isScrolling = 1;
isScrolling2 = 1;

nBottle = 0;
boxFilling = 0;

% Server initialization
vrep=remApi('remoteApi');
vrep.simxFinish(-1);
clientID=vrep.simxStart('127.0.0.1',19997,true,true,5000,5);

% Check connection
if (clientID>-1)
    disp('Connected to remote API server');
    
    % Get objects handles
    [res,proximity1]=vrep.simxGetObjectHandle(clientID,'Proximity_sensor_1',vrep.simx_opmode_blocking);
    [res,proximity_box]=vrep.simxGetObjectHandle(clientID,'Proximity_sensor_box',vrep.simx_opmode_blocking);
    [res,targetHandle]=vrep.simxGetObjectHandle(clientID,'target',vrep.simx_opmode_blocking);
    
    [bottleHandle, bottleModelHandle] = newObj(vrep, clientID, 'BottleDynamics', 'BottleModel', bottleDynamicsCoords, bottleDynamicsOrientation, bottleCoords, bottleOrientation);
    [res,boxHandle]=vrep.simxGetObjectHandle(clientID,'BoxDynamics',vrep.simx_opmode_blocking);
    [res] =vrep.simxSetObjectPosition(clientID, boxHandle, -1, boxDynamicsCoords, vrep.simx_opmode_blocking);
    
    % Init conveyour
    setConveyourVelocity(vrep, clientID, 'conveyorBelt_1_Velocity', 1, 0.05);
    setConveyourVelocity(vrep, clientID, 'conveyorBelt_2_Velocity', 1, 0.05);
    
    % Init actuator capping
    actuatorPosition=-0.05;
    setActuatorPosition(vrep, clientID, 'actuatorValue', actuatorPosition);
    gripper(vrep, clientID, 0, 'capping');
    
    % Init moving arm
    vrep.simxSetObjectPosition(clientID, targetHandle, -1, coords(1,1:3), vrep.simx_opmode_blocking);
else
    disp('Failed connecting to remote API server');
end   

% Cycle iteration
while true
    % If 
    if (isCapping==0) && (isMoving~=1)
        [res, detected_1, detected_point_1, detected_obj_1]=vrep.simxReadProximitySensor(clientID,proximity1,vrep.simx_opmode_blocking); 
        if detected_1 > 0 && lastCapped ~= detected_obj_1
            setConveyourVelocity(vrep, clientID, 'conveyorBelt_1_Velocity', 1, 0);  % Stop conveyour belt
            if lastCapped ~= -1
                isMoving = 1;
            end
            lastCapped = detected_obj_1;
            isCapping = 1;
            isScrolling = 0;
            
            nBottle = nBottle +1;
            bottleToPick(nBottle) = bottleHandle;
            bottleModelToPick(nBottle) = bottleModelHandle;
            [bottleHandle, bottleModelHandle] = newObj(vrep, clientID, 'BottleDynamics', 'BottleModel', bottleDynamicsCoords, bottleDynamicsOrientation, bottleCoords, bottleOrientation);
            vrep.simxSetObjectPosition(clientID, bottleHandle,-1, bottleDynamicsCoords+[0, 0, 0.0001], vrep.simx_opmode_blocking);
        elseif isScrolling == 0
            isScrolling = 1;
            setConveyourVelocity(vrep, clientID, 'conveyorBelt_1_Velocity', 1, 0.05);
        end
    end
    if isCapping == 1 && isScrolling2 == 0
        [actuatorPosition, isCapping, startCapping, step] = capping(vrep, clientID, step, isCapping, actuatorPosition, startCapping);
    end
    if isMoving ~= 0 && isScrolling2 == 0
        [stepMoving, isMoving, boxFilling, arrived] = moving(vrep, clientID, targetHandle, bottleToPick(pickedBottle+1), stepMoving, isMoving, boxFilling, coords, bottleBoxCoords(boxFilling+1,:));
        if arrived == 2
           pickedBottle = pickedBottle +1; 
        end
    end
    
    % Box conveyour belt
    if boxFilling == 4
        setConveyourVelocity(vrep, clientID, 'conveyorBelt_2_Velocity', 1, 0.05);
        isScrolling2 = 1;
        [res,detected_obj_2]=vrep.simxReadProximitySensor(clientID,proximity_box,vrep.simx_opmode_blocking);
        if(detected_obj_2 == 0)
           objToDelete = [ bottleToPick(pickedBottle-3:pickedBottle) bottleModelToPick(pickedBottle-3: pickedBottle) ];
           destroyObjects(vrep, clientID, objToDelete);
           boxFilling = 0;
           [res] =vrep.simxSetObjectPosition(clientID, boxHandle, -1, boxDynamicsCoords, vrep.simx_opmode_blocking);
        end
    end
     
    if isScrolling2 == 1 && boxFilling ~= 4
        [res,detected_obj_2]=vrep.simxReadProximitySensor(clientID,proximity_box,vrep.simx_opmode_blocking); 
        if(detected_obj_2 > 0)
            setConveyourVelocity(vrep, clientID, 'conveyorBelt_2_Velocity', 1, 0);
            isScrolling2 = 0;
        end
    end
   
end