function gripper(vrep, clientID, closing, label)
[s1,j1] = vrep.simxGetObjectHandle(clientID,strcat('ROBOTIQ_85_active1_',label), vrep.simx_opmode_oneshot_wait);
[s2,j2] = vrep.simxGetObjectHandle(clientID,strcat('ROBOTIQ_85_active2_',label), vrep.simx_opmode_oneshot_wait);
disp(j1)
[r,p1] = vrep.simxGetJointPosition(clientID, j1, vrep.simx_opmode_blocking);
[r,p2] = vrep.simxGetJointPosition(clientID, j1, vrep.simx_opmode_blocking);
if (closing==1)
    if (p1 < (p2-0.008))
        vrep.simxSetJointTargetVelocity(clientID, j1, -0.01, vrep.simx_opmode_blocking);
        vrep.simxSetJointTargetVelocity(clientID, j2, -0.04, vrep.simx_opmode_blocking);
    else
        vrep.simxSetJointTargetVelocity(clientID, j1, -0.04, vrep.simx_opmode_blocking);
        vrep.simxSetJointTargetVelocity(clientID, j2, -0.04, vrep.simx_opmode_blocking);
    end
else
    if (p1<p2)
        vrep.simxSetJointTargetVelocity(clientID, j1, 0.04, vrep.simx_opmode_blocking);
        vrep.simxSetJointTargetVelocity(clientID, j2, 0.02, vrep.simx_opmode_blocking);
    else
        vrep.simxSetJointTargetVelocity(clientID, j1, 0.02, vrep.simx_opmode_blocking);
        vrep.simxSetJointTargetVelocity(clientID, j2, 0.04, vrep.simx_opmode_blocking);
    end
end