function [status] = setConveyourVelocity(vrep, clientID, signalID, direction, velocity)
      vrep.simxSetFloatSignal(clientID, signalID, direction*velocity, vrep.simx_opmode_oneshot_wait);  
end