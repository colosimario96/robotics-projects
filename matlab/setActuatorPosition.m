function [activeActuator] = setActuatorPosition(vrep, clientID, signalID, position)
      vrep.simxSetFloatSignal(clientID, signalID, position, vrep.simx_opmode_oneshot_wait);  
end