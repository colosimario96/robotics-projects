function [ObjHandle, ObjModelHandle] = newObj(vrep, clientID, dynamicsObjectID, modelObjectID, coords1, orientation1, coords2, orientation2)
    [res, ObjDynamicsHandle] = vrep.simxGetObjectHandle(clientID, dynamicsObjectID, vrep.simx_opmode_oneshot_wait);
    [res, ObjHandle] = vrep.simxGetObjectHandle(clientID, modelObjectID, vrep.simx_opmode_oneshot_wait);
    [res, newObjHandles] = vrep.simxCopyPasteObjects(clientID, ObjHandle, vrep.simx_opmode_blocking);
    [res, newObjDynamicsHandles] = vrep.simxCopyPasteObjects(clientID, ObjDynamicsHandle, vrep.simx_opmode_blocking);
    
    [res] = vrep.simxSetObjectOrientation(clientID, newObjDynamicsHandles(1), -1, orientation1, vrep.simx_opmode_blocking);
    [res] = vrep.simxSetObjectPosition(clientID, newObjDynamicsHandles(1),-1, coords1, vrep.simx_opmode_blocking);
    
    [res] = vrep.simxSetObjectParent(clientID, newObjHandles(1), newObjDynamicsHandles(1), false, vrep.simx_opmode_blocking);
    [res] = vrep.simxSetObjectOrientation(clientID, newObjHandles(1), newObjDynamicsHandles(1), orientation2, vrep.simx_opmode_blocking);
    [res] = vrep.simxSetObjectPosition(clientID, newObjHandles(1), newObjDynamicsHandles(1), coords2, vrep.simx_opmode_blocking);
    ObjHandle = newObjDynamicsHandles(1);
    ObjModelHandle = newObjHandles(1);
end