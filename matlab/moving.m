function [step, isMoving, boxFilling, arrived] = moving(vrep, clientID, target, bottleHandle, step, isMoving, boxFilling, coords, bottleCoords)
    if step == 0
       arrived = moveL(vrep, clientID, target, coords(2,:), 2);
       if arrived == 1
           [res, attachHandle] = vrep.simxGetObjectHandle(clientID, 'ROBOTIQ_85_attachPoint#0', vrep.simx_opmode_oneshot_wait);
           vrep.simxSetObjectParent(clientID, bottleHandle, attachHandle, true, vrep.simx_opmode_blocking);
           step = 1;
           isMoving = 2;
       end
    elseif step == 1
        arrived = moveL(vrep, clientID, target, coords(3,:), 2);
        if arrived == 1
           step = 2; 
        end
   elseif step == 2
        arrived = moveL(vrep, clientID, target, bottleCoords, 2);
        if arrived == 1
           vrep.simxSetObjectParent(clientID, bottleHandle, -1, true, vrep.simx_opmode_blocking);
           step = 3; 
        end
   elseif step == 3
        arrived = moveL(vrep, clientID, target, coords(1,:), 2) + 1;
        if arrived == 2
           isMoving = 0;
           boxFilling = boxFilling + 1;
           step = 0; 
        end
    end
    
end