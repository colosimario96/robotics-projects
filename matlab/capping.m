function [actuatorPosition, isCapping, startCapping, step] = capping(vrep, clientID, step, isCapping, actuatorPosition, startCapping)
    if step == 0 
        if actuatorPosition < 0.003
            actuatorPosition = actuatorPosition + 0.005;
            vrep.simxSetFloatSignal(clientID, 'actuatorValue', actuatorPosition, vrep.simx_opmode_oneshot_wait);
        else
            step = 1;
        end
        startCapping = -1;
    elseif step == 1
        gripper(vrep, clientID, 1, 'capping');
        startCapping = tic;
        step = 2;
    elseif step == 2 && toc(startCapping) > 1
        gripper(vrep, clientID, 0, 'capping');
        startCapping = tic;
        step = 3;
    elseif step == 3 && toc(startCapping) > 1
        step = 4;
    elseif step == 4
        if actuatorPosition > -0.05
            actuatorPosition = actuatorPosition - 0.005;
            vrep.simxSetFloatSignal(clientID, 'actuatorValue', actuatorPosition, vrep.simx_opmode_oneshot_wait);
        else
            isCapping = 0;
            startCapping = -1;
            step = 0;
        end
    end
end
