function [arrived] = moveL(vrep, clientID, target, pos, speed)
    [r,p] = vrep.simxGetObjectPosition(clientID, target, -1, vrep.simx_opmode_blocking);
    [r,o] = vrep.simxGetObjectOrientation(clientID, target, -1, vrep.simx_opmode_blocking);
    %calculate shortest direction (positive or negative)
    for i=1:3
        if((abs(pos(i+3)-o(i))>pi) && (o(i)<0))
            o(i) = o(i) + 2*pi;
        elseif ((abs(pos(i+3)-o(i))>pi) && (o(i)<0))
            o(i) = o(i) - 2*pi;
        end
    end
    old_pos = [p o];
    delta_pos = pos - old_pos;
    distance = norm(delta_pos);
    samples_number = round(distance*50);
    if norm(p-pos(1:3)) > 0.001
        intermediate_pos = old_pos + (delta_pos/samples_number);
        vrep.simxSetObjectPosition(clientID, target, -1, intermediate_pos, vrep.simx_opmode_oneshot_wait);
        vrep.simxSetObjectOrientation(clientID, target, -1, intermediate_pos(4:6), vrep.simx_opmode_oneshot_wait);
        arrived = 0;
    else
        arrived = 1;
    end
end