function destroyObjects(vrep, clientID, objectsArray)
   for i=1:length(objectsArray)
      vrep.simxRemoveObject(clientID,objectsArray(i),vrep.simx_opmode_blocking);
   end
end